# cookiecutter-scrapy

Scrapy starter project for [Cookiecutter](https://github.com/audreyr/cookiecutter).

## Quick Start

Install Cookiecutter globally:

```sh
python3 -m pip install --upgrade cookiecutter
```

Generate the boilerplate:

```sh
cookiecutter https://gitlab.com/victor.kirillov/cookiecutter-scrapy.git
```

## Parameters

| Name | Description |
| :--- | :----------------- |
| project_name | Scrapy project name. Use in app_name variable |
| app_name | Сleaned project_name variable. Use for creating project |
